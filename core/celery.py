import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab



os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")

app = Celery("core")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.conf.beat_schedule = {
    "send_email_report": {
        "task": "core.tasks.send_email_report",
        "schedule": crontab(minute="*/1"),
    },
}
app.autodiscover_tasks()