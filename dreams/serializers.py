from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username")


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField()


class QuestionSerializer(serializers.ModelSerializer):
    human = serializers.SlugRelatedField(slug_field="username", read_only=True)

    class Meta:
        model = Question
        fields = ("id", "description", "date", "human")


class QuestionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ("description", "human")


class ExpertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expert
        fields = ("id", "name", "prof")


class AnswerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ("question", "description", "human")


class AnswerSerializer(serializers.ModelSerializer):
    human = serializers.SlugRelatedField(slug_field="name", read_only=True)
    question = serializers.SlugRelatedField(slug_field="description", read_only=True)

    class Meta:
        model = Answer
        fields = ("id", "question", "description", "human", "date")


class TestCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ("name", "description", "human", "link")


class TestSerializer(serializers.ModelSerializer):
    human = serializers.SlugRelatedField(slug_field="name", read_only=True)

    class Meta:
        model = Test
        fields = ("id", "name", "description", "human", "link", "date")


# ARTICLES
class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ("id", "title", "content", "category")


class ArticleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ArticleCategory
        fields = ("id", "name")


class ArticleCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ("title", "content", "category")


class ArticleCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArticleCategory
        fields = ("name",)


class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "category", "title", "description"]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "category", "title", "description"]


class CommentCreateSerializer(serializers.ModelSerializer):
    # post=serializers.SlugRelatedField(slug_field="title", read_only=True )
    message = serializers.CharField()

    class Meta:
        model = Comment
        fields = ["name", "email", "message", "product"]

    def validate_message(self, value):
        if len(value) < 10:
            raise serializers.ValidationError(
                f"Hазвание должно быть минимум 10 символов, вы ввели {len(value)}"
            )
        return value
