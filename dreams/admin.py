from django.contrib import admin
from .models import *
from django.utils.safestring import mark_safe

class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 1

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ("human", "description",)
    list_filter = ("human", "date",)
    search_fields = ("description__startswith",)
    inlines = [AnswerInline]

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["id","title", "category"]
    save_as = True #Сохранить как новый объект, чтобы нам не вписывать каждый раз новый пост, а просто его копировать
    save_on_top = True#Кнопка сохранить в админке будет и наверху, чтобы нам не листать вниз
    list_filter = ["category"]
    search_fields = ["category__name__startswith"]

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'create_at', 'id']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "slug")

@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ("question", "description",)
    list_filter = ("question", "date", "human",)
    search_fields = ("description__startswith",)

@admin.register(Expert)
class ExpertAdmin(admin.ModelAdmin):
    list_display = ("name", "prof")
    list_filter = ("prof",)
    search_fields = ("prof__startswith",)

@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ("name", "description",)
    list_filter = ("human", "name",)
    search_fields = ("name__startswith",)
    
#ARTICLES


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'content', 'category', 'author', 'get_html_photo', 'is_published')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'content')
    list_editable = ('is_published',)
    list_filter = ('is_published', )
    save_on_top = True
    
    readonly_fields = ('get_html_photo',)
    
    def get_html_photo(self, object):
        if object.image:
            return mark_safe(f"<img src='{object.image.url}' width=100>")
    
    get_html_photo.short_description = "Миниатюра"
    
    
class ArticleCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')
    search_fields = ('name',)
    
admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleCategory, ArticleCategoryAdmin)