from datetime import timedelta, time, datetime
from django.core.mail import mail_admins
from django.utils import timezone
from django.utils.timezone import make_aware
from .models import Question
from django.core.cache import cache
from django.conf import settings


def send_report():
    today = timezone.now()
    tomorrow = today + timedelta(1)
    today_start = make_aware(datetime.combine(today, time()))
    today_end = make_aware(datetime.combine(tomorrow, time()))

    questions = Question.objects.filter(date__range=(today_start, today_end))

    if questions:
        message = ""

        for question in questions:
            message += f"{question} \n"

        subject = (
            f"Question Report for {today_start.strftime('%Y-%m-%d')} "
            f"to {today_end.strftime('%Y-%m-%d')}"
        )

        mail_admins(subject=subject, message=message, html_message=None)

def delete_cache(key_prefix: str):
    """
    Delete all cache keys with the given prefix.
    """
    keys_pattern = f"views.decorators.cache.cache_*.{key_prefix}.*.{settings.LANGUAGE_CODE}.{settings.TIME_ZONE}"
    cache.delete_pattern(keys_pattern)
