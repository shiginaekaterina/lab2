from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register(r"user", UserViewSet)
router.register(r"questions", QuestionViewSet)
router.register(r"answers", AnswerViewSet)
router.register(r"tests", TestViewSet)
router.register(r"experts", ExpertViewSet)
# articles
router.register(r"articles", ArticleViewSet)
router.register(r"article-category", ArticleCategoryViewSet)
router.register(r"product", ProductViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("product/comment/<int:pk>/", CreateAPIComment.as_view(), name="post_comment"),
]
