from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone


class Question(models.Model):
    description = models.CharField(verbose_name="детали вопроса", max_length=255)
    date = models.DateField(verbose_name="дата вопроса", auto_now_add=True)
    human = models.ForeignKey(
        User,
        verbose_name="автор вопроса",
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"


class Expert(models.Model):
    name = models.CharField(verbose_name="имя", max_length=255)
    prof = models.CharField(verbose_name="сфера деятельности", max_length=255)

    def __str__(self):
        return f"{self.name}, {self.prof}"

    class Meta:
        verbose_name = "Эксперт"
        verbose_name_plural = "Эксперты"


class Answer(models.Model):
    question = models.ForeignKey(
        to=Question, on_delete=models.CASCADE, verbose_name="вопрос"
    )
    description = models.CharField(verbose_name="детали ответа", max_length=255)
    date = models.DateField(verbose_name="дата ответа", auto_now_add=True)
    human = models.ForeignKey(
        to=Expert, on_delete=models.CASCADE, verbose_name="создатель"
    )

    def __str__(self):
        return f"{self.description}, {self.human}"

    class Meta:
        verbose_name = "Ответ"
        verbose_name_plural = "Ответы"


class Test(models.Model):
    name = models.CharField(verbose_name="название теста", max_length=255)
    description = models.CharField(verbose_name="описание теста", max_length=255)
    date = models.DateField(verbose_name="дата создания", auto_now_add=True)
    link = models.URLField(verbose_name="ccылка на тест", max_length=255)
    human = models.ForeignKey(
        to=Expert, on_delete=models.CASCADE, verbose_name="создатель"
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тест"
        verbose_name_plural = "Тесты"


class Article(models.Model):
    title = models.CharField("Заголовок", max_length=255)
    content = models.TextField("Содержание")
    author = models.CharField("Автор", max_length=255)
    publication_date = models.DateField("Дата публикации", auto_now_add=True)
    image = models.ImageField("Изображение", upload_to="article_images/")
    category = models.ForeignKey(
        "ArticleCategory", on_delete=models.PROTECT, null=True, verbose_name="Категория"
    )
    is_published = models.BooleanField("Опубликовано", default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"


class ArticleCategory(models.Model):
    name = models.CharField(max_length=255, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория Статьи"
        verbose_name_plural = "Категории Статей"


class Category(models.Model):
    name = models.CharField("Название", max_length=100)
    slug = models.SlugField("УФ URL", max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория Продукта"
        verbose_name_plural = "Категории Продуктов"


class Product(models.Model):
    title = models.CharField("Название", max_length=200)
    description = models.TextField("Описание")
    price = models.TextField("Цена")
    image = models.ImageField("Картинка", upload_to="products/", null=True, blank=True)
    category = models.ForeignKey(
        Category,
        related_name="product",
        verbose_name="Категории",
        on_delete=models.SET_NULL,  ##Удаляем категорию, а продукт остается
        null=True,
    )

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(
            "post_single", kwargs={"slug": self.category.slug, "post_slug": self.slug}
        )

    def get_comments(self):
        return self.comment.all()


class Comment(models.Model):
    name = models.CharField("Имя", max_length=50)
    email = models.CharField("Эмэил", max_length=100)
    message = models.TextField("Сообщение", max_length=500)
    create_at = models.DateTimeField("Дата коммента", default=timezone.now)
    product = models.ForeignKey(
        Product,
        related_name="comment",
        verbose_name="Продукт",
        on_delete=models.CASCADE,
        null=True,
    )  ##Удаляе кпост удаляются и комментарии

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
