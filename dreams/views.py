from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import permissions
from .models import *
from .serializers import *
from .permissions import IsAdminOrReadOnly
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from dreams.utils import send_report
from dreams.utils import delete_cache
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(username=user.username)

    def get_serializer_class(self):
        if self.action == 'set_password':
            return PasswordSerializer
        return UserSerializer
    
    @action(detail=True, methods=['post'])
    def set_password(self, request, pk=None):
        user = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user.set_password(serializer.validated_data['password'])
            user.save()
            return Response({'status': 'password set'})
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class QuestionViewSet(viewsets.ModelViewSet):
    
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    
    def get_serializer_class(self):
        if self.action in ['create']:
            return QuestionCreateSerializer
        return QuestionSerializer
    
    
    
    @action(detail=False)
    def send_report(self, request):
        send_report()
        return Response(status=status.HTTP_200_OK)


class ExpertViewSet(viewsets.ModelViewSet):
    
    serializer_class = ExpertSerializer
    queryset = Expert.objects.all()
    permission_classes = [permissions.IsAdminUser]

class AnswerViewSet(viewsets.ModelViewSet):
    
    serializer_class = AnswerSerializer
    queryset = Answer.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    
    def get_serializer_class(self):
        if self.action in ['create']:
            return AnswerCreateSerializer
        return AnswerSerializer

    CACHE_KEY_PREFIX = "answer-view"

    @method_decorator(cache_page(300, key_prefix=CACHE_KEY_PREFIX))
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        delete_cache(self.CACHE_KEY_PREFIX)
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        delete_cache(self.CACHE_KEY_PREFIX)
        return response

    def partial_update(self, request, *args, **kwargs):
        response = super().partial_update(request, *args, **kwargs)
        delete_cache(self.CACHE_KEY_PREFIX)
        return response

class TestViewSet(viewsets.ModelViewSet):
    
    serializer_class = TestSerializer
    queryset = Test.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    
    def get_serializer_class(self):
        if self.action in ['create']:
            return TestCreateSerializer
        return TestSerializer

#articles

class ArticleViewSet(viewsets.ModelViewSet):
    
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    
    def get_serializer_class(self):
        if self.action in ['create']:
            return ArticleCreateSerializer
        return ArticleSerializer

class ArticleCategoryViewSet(viewsets.ModelViewSet):
    serializer_class = ArticleCategorySerializer
    queryset = ArticleCategory.objects.all()
    
    def get_serializer_class(self):
        if self.action in ['create']:
            return ArticleCategoryCreateSerializer
        return ArticleCategorySerializer


class CreateAPIComment(ListCreateAPIView):
    def get_queryset(self):
        return Comment.objects.filter(product__id=self.kwargs.get("pk")).select_related('product')
    model = Comment
    serializer_class = CommentCreateSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )

    def perform_create(self, request):
        serializer = CommentCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class ProductViewSet(viewsets.ModelViewSet):
    # serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.action in ['create']:
            return ProductCreateSerializer
        return ProductSerializer

    @action(detail=True, methods=['post'])
    def file_create(self, request, pk=None):
        project = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            project.file_project = serializer.validated_data['text']
            project.save()
            return Response({'status': 'text успешно изменен'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False)
    def projects(self, request):
        projects = Product.objects.all()
        return Response({'product': [product.name for product in projects]})